import axios from 'axios'
import csvToJson from 'convert-csv-to-json'

const GeoJson = async (req, res) => {
    function toJson(csv) {
        const geoArray = JSON.stringify(csv.toString()).replace(/"/g,'')
        const splitArray = geoArray.split("\\n")
        const json = splitArray.reduce((accumulator, currentValue) => {
            const split = currentValue.split(',')
            if(split[0] != 'id' && split[0] !== '') {
                accumulator.push({ id: split[0], name: split[1] })
            } else {
                accumulator
            }
            return accumulator
        }, [])
        return json
    }

    try {
        
        const { data } = await axios.get('https://cswcl.github.io/fake-api/monumentos_historicos_extracto.geojson');
        const geoCsv = await axios.get('https://cswcl.github.io/fake-api/monumentos_historicos_extracto.csv')
        
        const result = data.features.map(entry => {
            const name = toJson(geoCsv.data).filter(item => {
                if(entry.properties.id == item.id) return item.name
            })
            entry.properties.name = name[0].name
            return entry
        })

        return res.json(result)
        
    } catch (error) {
        console.error(error);
    }
    
    
    
}

export default GeoJson