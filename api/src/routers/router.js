import { Router } from 'express'
import Controller from '../controllers/controller'

const router = Router()

router.get('/api/gjson', Controller)

export default router