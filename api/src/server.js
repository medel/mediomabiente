import express from 'express'
import cors from 'cors'
import router from './routers/router'

const app = express()

// Settings
app.use(cors())

// Routes
app.use(router)


export default app