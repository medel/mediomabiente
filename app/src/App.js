import React from 'react';
import './App.css';
import Menu from './components/menu'
import Body from './components/body'

function App() {
  return (
    <div className="row">
      <Menu/>
      <Body/>
    </div>
  );
}

export default App;
