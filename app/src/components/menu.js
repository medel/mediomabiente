import React from 'react';

function Menu() {
  return (
    <div className="col-2 menu-container">
        <div className="side-head">
            <h3 className="title-flax">MedioAmbiente</h3>
        </div>
        <ul className="list-group">
            <li className="list-group-item list-group-item-action">Creación</li>
            <li className="list-group-item list-group-item-action">Edición</li>
            <li className="list-group-item list-group-item-action">Geometría</li>
            <li className="list-group-item list-group-item-action">Importar</li>
            <li className="list-group-item list-group-item-action">Exportar</li>
        </ul>
        
    </div>
  );
}

export default Menu;