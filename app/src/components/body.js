import React from 'react'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import userSWR from 'swr'

const fetcher = (...args) => fetch(...args).then(response => response.json())

function Maker() {

    const url = "http://localhost:3000/api/gjson";    

    const { error, data } = userSWR(url, fetcher)

    if(error) return console.log("Los datos no fueron cargados")
    if(!data) return []

    return data;


}


function Body() {
    const position = [-33.438441264, -70.649550196]

    const positions = Maker().map((pos, index) => {
        return (
            <Marker key={index} position={[pos.geometry.coordinates[1] , pos.geometry.coordinates[0]]}>
                <Popup>
                    { pos.properties.name }
                </Popup>
            </Marker>
        )
    })

    return (
        <div className='col-10 body-container'>
            <Map center={position} zoom={17}>
                <TileLayer
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                />

                { positions }
                
            </Map>
        </div>
    )
}

export default Body